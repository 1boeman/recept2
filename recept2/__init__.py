import os

from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'recept2.sqlite'),
        SOLR='http://localhost:8983/solr',
        SOLR_API='http://localhost:8983/api',
        SOLRCORE='recept',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    from . import solr
    solr.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import recept
    app.register_blueprint(recept.bp)
    app.add_url_rule('/', endpoint='index')

    from . import search
    app.register_blueprint(search.bp)
 

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app

