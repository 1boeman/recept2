import json
import requests
import click
import xml.etree.ElementTree as ET
from flask import current_app, g
from flask.cli import with_appcontext

from recept2.recept import get_all

def init_app(app):
    app.cli.add_command(init_solr_command)
    app.cli.add_command(index_solr_command)


def init_solr():
    with current_app.open_resource('solr_schema.xml') as f:
        root = ET.fromstring(f.read()) 
        for field in root:
            if field.tag == 'field':
                solr_schema_request("delete-field",{"name" : field.attrib['name']})
                solr_schema_request("add-field",field.attrib)
            elif field.tag == 'copy-field':
                solr_schema_request("delete-copy-field",field.attrib)
                solr_schema_request("add-copy-field",field.attrib)

    
def select(querystring = b'', params = {} ):
    solr = get_solr()
    solr = "/".join([solr,'select'])
    if len(querystring): 
        r = requests.get(solr,querystring)
    elif params:
        r = requests.get(solr,params) 

    return r.json()


def index_solr():
    recepten = get_all() 
    for r in recepten:
        index_doc (r)


def index_doc(data):
    data = dict(data)   
    data['created'] = data['created'].isoformat() 
    if 'Z' not in data['created']:
        data['created'] = data['created'] + 'Z'
    solr = get_solr()
    solr = "/".join([solr,"update"])
    command = {"add":{"doc":data},"commit":{}}
    r = requests.post(solr,json=command )
    return r


def delete_doc(id):
    solr = get_solr()
    solr = "/".join([solr,"update"])
    command = {"delete":{"id":id},"commit":{}}
    r = requests.post(solr,json=command )


def solr_schema_request(command,data):
    solr_schema = get_solr_schema()
    print (solr_schema)
    call =  {command:data}
    r = requests.post(solr_schema,json=call )
    print (call)
    print (r)


@click.command('init-solr')
@with_appcontext
def init_solr_command():
    init_solr()
    click.echo('Initialized solr schema')


@click.command('index-solr')
@with_appcontext
def index_solr_command():
    index_solr()
    click.echo('Indexed all data in solr')


def get_solr_schema ():
    if 'solr_schema' not in g:
        g.solr_schema = "/".join([current_app.config['SOLR_API'],'cores',current_app.config['SOLRCORE'],'schema'])
        
    return g.solr_schema


def get_solr ():
    if 'solr' not in g:
        g.solr = "/".join([current_app.config['SOLR'],current_app.config['SOLRCORE']])
        
    return g.solr 
