import markdown
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
import json
from recept2.auth import login_required
from recept2.db import get_db
import recept2.solr 

bp = Blueprint('recept', __name__)

@bp.route('/')
def index():
   posts = get_all()
   return render_template('recept/index.html', posts=posts)

def get_all():
    db = get_db()
    posts = db.execute(
        'SELECT u.username, r.*'
        ' FROM recepten r JOIN user u ON r.auteur_id = u.id'
        ' ORDER BY r.created DESC'
    ).fetchall()

    return posts
 

@bp.route('/recept/<int:id>', methods=('GET',))
def recept(id):
    result = get_recept(id,check_author=False)
    post = dict(result)
    post['ingredienten'] = markdown.markdown(post['ingredienten'])
    post['bereiding'] = markdown.markdown(post['bereiding'])

    return render_template('recept/recept.html', post=post)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        form = request.form
        values = [g.user['id'],form['titel'], form['ingredienten'],form['bereiding'], form['tijd'], form['personen']]
        error = None

        if not form['titel']:
            error = 'Titel is een moetje.'

        if error is not None:
            flash(error)
        else:
            # database
            db = get_db()
            cursor = db.cursor()
            cursor.execute('''
                  insert into recepten
                  (auteur_id,titel,ingredienten,bereiding,tijd,personen) 
                  VALUES ( ?,?,?,?,?,? )''',values
            )
            db.commit()
           
             #solr
            last_row_id = cursor.lastrowid
            row = get_recept(last_row_id)
            recept2.solr.index_doc(row) 

            return redirect(url_for('recept.index'))

    return render_template('recept/create.html')

def get_recept(id, check_author=True):
    post = get_db().execute(
        'SELECT u.username, r.* ' 
        ' FROM recepten r JOIN user u ON r.auteur_id = u.id'
        ' WHERE r.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Recept met id {0} doet niet bestanie niet.".format(id))

    if check_author and post['auteur_id'] != g.user['id']:
        abort(403)

    return post

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_recept(id)

    if request.method == 'POST':
        form = request.form
        values = [form['titel'], form['ingredienten'],form['bereiding'], form['tijd'], form['personen'], post['id']]

        error = None

        if not form['titel']:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                ''' update recepten
                    set titel = ?, ingredienten = ?, bereiding = ?,  tijd = ?, personen = ?
                      where id = ? ''', values
            )
            db.commit()

            row = get_recept(id)
            recept2.solr.index_doc(row) 

            return redirect(url_for('recept.index'))

    return render_template('recept/update.html', post=post)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_recept(id)
    db = get_db()
    db.execute('DELETE FROM recepten WHERE id = ?', (id,))
    db.commit()
    
    recept2.solr.delete_doc(id) 

    return redirect(url_for('recept.index'))


